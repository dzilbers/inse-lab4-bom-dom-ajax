const path = require('path');
const express = require('express');
const app = express();

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

let drinks = [ { name: 'Bloody Mary', drunkness:  3 },
               { name: 'Martini',     drunkness:  5 },
               { name: 'Scotch',      drunkness: 10 }
             ];

app.get('/', function(req, res) {
  let line = "Any your code that you haven't looked at for few months might have been written by someone else";
  res.render('index4', { drinks: drinks, tagline: line });
});

app.get('/drinks', function(req, res) {
  let names = [];
  drinks.forEach(function(drink) { names.push(drink.name) });
  res.json(names);
});

app.get('/drinks/:drinkId', function(req, res) {
  res.json(drinks[req.params.drinkId]);
});

app.get('/about', function(req, res) {
  res.render('about');
});

app.listen(8080, ()=>{console.log('8080 is the magic port');});
